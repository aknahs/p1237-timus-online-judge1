#define XCOORD 0
#define YCOORD 1
#define CAPACITY 2

#define NSTATES 4
#define STATE_COVER 0
#define STATE_STAR 1
#define STATE_PRIME 2
#define STATE_ID 3

#define UNCOVERED 0
#define COVERED 1

#define NOT_PRIME -1
#define NOT_STAR -1

#define FINISHED 1
#define NOT_FINISHED 0

typedef struct lista_el{
  int i;
  int j;
  struct lista_el *next;
} lista;

void insert_list(lista **lst,int i,int j);
void print_list(lista *lst);
void free_list(lista *lst);

void init_matrix(int **mtx,int **,int **, int n, int m);
void erase_primes_and_uncover_lines(int **mtx, int n, int m);
void free_matrix(int **mtx, int n);
void print_matrix(int **mtx, int n, int m);
void step1(int **mtx, int n, int m);
void step2(int **mtx, int n, int m);
void step3(int **mtx, int n, int m);
void step4(int **mtx, int n, int m);
void step5(int **mtx, int n, int m, int primei, int primej);
void step6(int **mtx, int n, int m, int min);
void solve_matrix(int **mtx,int n,int m);
int **rotate_matrix(int **mtx, int n, int m);
void remove_full_and_empty(int **mtx, int **buildings, int **shelters, int *n, int *m);
int getmincost(int **building, int **shelter,int n, int m);
void submincost(int **mtx, int **buildings, int **shelters,int min,int n, int m);
void send_people(int **res, int **mtx, int n, int m, int min);
