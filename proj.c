#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <strings.h>

#include "proj.h"

static int initial_n, initial_m;

/*
 * A matriz e N+3 x M+3 para guardar a informacao
 * de acerca do estado das linhas e colunas
 * N  e M  - (UN)CONVERED
 * N + 1 e M + 1 - indices na linha e coluna do zero STAR
 * N + 2 e M + 2 - indices na linha e coluna do zero PRIME
 *
 */

void insert_list(lista **lst,int i,int j){
  lista *elem = malloc(sizeof(lista));
  elem->i = i;
  elem->j = j;
  elem->next = *lst;
  *lst = elem;

}

void print_list(lista *lst){
  for(;lst;lst=lst->next)
    printf("%d %d\n",lst->i,lst->j);
}

void free_list(lista *lst){
  lista *elem = lst;
  if(lst==NULL) {return;}
  for(lst=lst->next; lst; elem=lst,lst=lst->next)
    free(elem);
  free(elem);
}

void init_result(int **mtx, int n, int m){
  int i;
  for(i=0;i<n;i++)
    bzero(mtx[i],sizeof(int)*m);
}

void init_matrix(int **mtx,int **buildings,int **shelters,int n, int m){
  int i,j,iaux,jaux,k;

  /*init_result(mtx,n+4,m+4);*/
  /*linhas*/
  for(i=0;i<n;i++){
    mtx[i][m]=UNCOVERED;
    mtx[i][m+1]=NOT_STAR;
    mtx[i][m+2]=NOT_PRIME;
  }
  
  /*colunas*/
  for(j=0;j<m;j++){
    mtx[n][j]=UNCOVERED;
    mtx[n+1][j]=NOT_STAR;
    mtx[n+2][j]=NOT_PRIME;
  }

  /*Espaço nao utilizado*/
  for(i=n;i<n+4;i++){
    mtx[i][m]=0;
    mtx[i][m+1]=0;
    mtx[i][m+2]=0;
    mtx[i][m+3]=0;
  }

  /*Atribuicao de ids unicos*/
  for(i=0,iaux=0;i<n;iaux++,i+=k)
    for(k=0;k<buildings[iaux][2];k++)
      mtx[i+k][m+3] = iaux;
  for(j=0,jaux=0;j<m;jaux++,j+=k)
    for(k=0;k<shelters[jaux][2];k++)
      mtx[n+3][j+k] = jaux+iaux;

  /*Preencher a matriz*/
 for(i=0,iaux=0;i<n;iaux++,i+=k){
    for(j = 0,jaux=0; j < m; jaux++,j+=k){
      mtx[i][j] =  abs(buildings[iaux][0] - shelters[jaux][0]) + abs(buildings[iaux][1] - shelters[jaux][1]) + 1;
      for(k=1;k<shelters[jaux][2];k++)
	mtx[i][j+k] = mtx[i][j];
    }
    for(k=1;k<buildings[iaux][2];k++)
      memcpy(mtx[i+k],mtx[i],sizeof(int)*(m+4));
  }

}

/* 
 * chamar se m < n
 */
int **rotate_matrix(int **mtx, int n, int m) {

  int **rotated, i, j;

  rotated = malloc(sizeof(int*)*(m+4));
  for(i = 0; i < m+4; i++)
    rotated[i] = malloc(sizeof(int)*(n+4));

  /*
   * mudar pra + 4
   */
  for(i = 0; i < n + 4; i++)
    for(j = 0; j < m + 4; j++)
      rotated[j][i] = mtx[i][j];

  free_matrix(mtx,n+4);

  return rotated;

}

void erase_primes_and_uncover_lines(int **mtx, int n, int m) {

  int i;

  for(i = 0; i < m; i++)
    mtx[n+2][i] = NOT_PRIME;

  for(i = 0; i < n; i++){
    mtx[i][m] = UNCOVERED;
    mtx[i][m+2] = NOT_PRIME;
  }
}

void free_matrix(int **mtx, int n){
  int i;
  for(i = 0; i < n; i++)
    free(mtx[i]); 
  free(mtx);  
}

void print_matrix(int **mtx, int n, int m){
  int i,j;
  printf("\nMatrix--------\n");
  for(i=0;i<n;i++){
    for(j=0;j<m;j++)
      printf("%d\t",mtx[i][j]);
    printf("\n");
  }
  printf("\n---------------\n");
} 


void print_result(int **mtx, int n, int m){
  int i,j;
  for(i=0;i<n;i++){
    for(j=0;j<m;j++)
      printf("%d ",mtx[i][j]);
    printf("\n");
  }
} 

/*Para cada linha da matriz encontra o menor elemento e subtrai-o 
  a todos os elementos dessa linha*/
void step1(int **mtx, int n, int m) {

  int min, i, j;
  
  /*  puts("step1");*/
  for(i = 0; i < n; i++) {
    min = mtx[i][0];
    
    for(j = 1; j < m; j++)
      if(mtx[i][j] < min) min = mtx[i][j];
    
    for(j = 0; j < m; j++)
      mtx[i][j] -= min;
  }
}

/*Escolhe um zero e faz star se nao houver outro zero com star na mesma linha ou coluna.
  Repete para todos os zeros*/
void step2(int **mtx, int n, int m) {
  int i,j;

  /*So procura os 0 na matrix n*m.*/
  for(i=0;i<n;i++)
    for(j=0;j<m;j++){
      if(mtx[i][j]==0 && mtx[i][m+1] == NOT_STAR && mtx[n+1][j] == NOT_STAR){
	mtx[i][m+1]=j;
	mtx[n+1][j]=i;
      }

    }
}

/*Risca todas as colunas que contenham o zero starred.
  Se todas as colunas estiverem riscadas a solucao esta achada.
  Caso contrario, continua os steps.*/
void step3(int **mtx, int n, int m) {
  
  int i, ncovered=0;
  
  /*queres e percorrer as colunas
    for(i = 0; i < n; i++)*/
  
  for(i = 0; i < m; i++){
    if(mtx[n+1][i] != NOT_STAR){ 
      mtx[n][i] = COVERED;
      ncovered++;
    }
  }
  /*Encontrou todas as solucoes? se sim acaba a execucao*/
  if(ncovered >= n)/*(m > n ? m : n)) --------------------------------------------*/
    return;
  /*Senao continua os steps*/
  step4(mtx,n,m);
}

/*
  Encontrar um 0 nao riscado e elege-lo (prime).
  Se nao houver um 0 starred na sua linha passar ao passo 5!
  Se houver um 0 starred na sua linha:
  -riscar essa linha.
  -desriscar a coluna do 0 starred.
  Continuar ate nao existirem zeros nao cobertos.
  Devolve o menor valor descoberto nao coberto e passa ao passo 6!
*/
void step4(int **mtx, int n, int m){
  int i,j,min=-1;
  
  /*  puts("step4");*/
  for(i=0,j=0;i<n;i++)
    /*A linha nao ta riscada*/
    if(mtx[i][m] == UNCOVERED){
      for(j=0;j<m;j++)
	/*a coluna tambem nao ta riscada*/
	if(mtx[n][j] == UNCOVERED){
	  /*verifica se e um minimo*/
	  if(min == -1)
	    min = mtx[i][j];
	  else
	    if(mtx[i][j] < min)
	      min = mtx[i][j];
	  
	  if(mtx[i][j]==0){
	    /*elege o 0*/
	    if(mtx[n+1][j] == i){
	      puts("KAPUTS");
	      printf("covercol %d coverline %d",mtx[n][j],mtx[i][m]);
	    }
	    mtx[n+2][j] = i;
	    mtx[i][m+2] = j;
	    
	    /*verifica se nao ha um 0 starred nessa linha*/
	    if(mtx[i][m+1]==NOT_STAR){
	      step5(mtx,n,m,i,j);
	      return; /*ep5() : passa ao passo 5*/
	    }
	    else{
	      mtx[i][m]=COVERED;
	      mtx[n][mtx[i][m+1]]=UNCOVERED;
	      break;
	    }
	  }
	}
    }
  step6(mtx,n,m,min);
  return;
}

/*Z0= uncovered primed zero do Step 4.  
  Z1= starred zero na coluna de Z0 (if any). 
  Z2= primed zero na linha de Z1 (there will always be one).  
  Continuar ate a serie termine num primed zero SEM STARRED ZERO na sua coluna.  
    - fazer unstar de todos os starreds zero das series.
    - fazer star dos 0 eleitos das series
    - apagar todos os eleitos
    - fazer uncover de todas as linhas da matriz
  Voltar ao step3
*/
void step5(int **mtx, int n, int m, int primei, int primej) {

  int z1i, z2j;

  lista* z0 = NULL;
  lista* z1 = NULL;
  lista* z2 = NULL;
  lista* aux;

  while(1) {
    
    insert_list(&z0,primei,primej);
    /*Se tiver um starred zero nessa coluna continua*/
    if(mtx[n+1][primej] != NOT_STAR) {/*---------------------------*/
      
      /*starred zero na coluna de z0*/
      z1i = mtx[n+1][primej];
      
      insert_list(&z1,z1i,primej);
    
      /*primed zero na linha de z1*/
      z2j = mtx[z1i][m+2];
    
      insert_list(&z2,z1i,z2j);

      primei = z1i;
      primej = z2j;
    }
    
    else /*NAO tem um starred zero nessa coluna*/
      break;
  }

  aux = z1;
  for(;aux;aux=aux->next) {
    mtx[n+1][aux->j] = NOT_STAR;
    mtx[aux->i][m+1] = NOT_STAR;
  }
 
  free_list(z1);

  aux = z0;
  for(;aux;aux=aux->next) {
    mtx[n+1][aux->j] = aux->i;
    mtx[aux->i][m+1] = aux->j;

    mtx[n+2][aux->j] = NOT_PRIME;
    mtx[aux->i][m+2] = NOT_PRIME;

  }

  free_list(z0);

  aux = z2;
  for(;aux;aux=aux->next) {
    mtx[n+1][aux->j] = aux->i;
    mtx[aux->i][m+1] = aux->j;

    mtx[n+2][aux->j] = NOT_PRIME;
    mtx[aux->i][m+2] = NOT_PRIME;

  }

  free_list(z2);

  erase_primes_and_uncover_lines(mtx,n,m);

  step3(mtx,n,m);
}

void step6(int **mtx, int n, int m, int min) {

  int i, j;
  /*  puts("step6");*/
  for(i = 0;i < n; i++)
    if(mtx[i][m] == COVERED)
      for(j = 0; j < m; j++)
	mtx[i][j] += min;

  for(j = 0;j < m; j++)
    if(mtx[n][j] == UNCOVERED)
      for(i = 0; i < n; i++)
	mtx[i][j] -= min;

  step4(mtx,n,m);
}

void send_people(int **res, int **mtx, int n, int m, int min) {

  int i, idi, idj, aux;

  for(i = 0;i < n; i++) {
    idi = mtx[i][m+3];
    idj = mtx[n+3][mtx[i][m+1]];
    
    if(idi < initial_n)
      idj -= initial_n;
    else {
      aux = idi - initial_n;
      idi = idj;
      idj = idi;
    }
    res[idi][idj] += min;
  }
}

void solve_matrix(int **mtx,int n,int m){
  step1(mtx,n,m);
  step2(mtx,n,m);
  step3(mtx,n,m);
}

int getmincost(int **building, int **shelter,int n, int m) {
  int i,min = INT_MAX;

  for(i=0;i<n;i++)
    if(building[i][2]<min && building[i][2])
      min=building[i][2];
  
  for(i=0;i<m;i++)
    if(shelter[i][2]<min && shelter[i][2])
      min=shelter[i][2];

  return min;
}
  
void submincost(int **mtx, int **buildings, int **shelters,int min,int n, int m) {
  int i,idi,idj;
  if(mtx[0][m+3]<initial_n){ /*linhas sao buildings*/
    for(i=0;i<n;i++){
      idi= mtx[i][m+3];
      buildings[idi][2]-=min;
      idj=mtx[n+3][mtx[i][m+1]] - initial_n;
      shelters[idj][2] -=min;
    }
  }
  else{ /*linhas sao shelters*/
    for(i=0;i<n;i++){
      idi= mtx[i][m+3] - initial_n;
      shelters[idi][2] -=min;
      idj=mtx[n+3][mtx[i][m+1]];
      buildings[idj][2]-=min;
    }
  }
}

void remove_full_and_empty(int **mtx, int **buildings, int **shelters, int *n, int *m) {
  
  int i, j, jaux, newn, newm;
  int **aux1, **aux2, **aux3;

  aux1 = aux2 = mtx;

  newn = *n;
  newm = *m;

  if(mtx[0][newm + 3] < initial_n) {
    /*
     * forma normal
     */
    
    /* remover linhas
     */
    for(i = 0;i < *n; i++){
      
      if(buildings[mtx[i][(*m)+3]][2] == 0) {
	--newn;
	aux3 = aux2++;
	free(*aux3);
      }
      else
	*aux1++ = *aux2++;
    }
    
    for(j = *n; j < (*n) + 4; j++)
      *aux1++ = *aux2++;
    
    
    /* for(;i < (*n) + 4; i++)
     * aux1++ = NULL;
     */

    
    /* remover colunas
     */
    for(j = 0, jaux = 0; j < *m; j++)
      
      if(shelters[mtx[(*n)+3][j]-initial_n][2] == 0)
	--newm;
      else {
	for(i = 0; i < newn + 4; i++)
	  mtx[i][jaux] = mtx[i][j];
	++jaux;
      }

    for(jaux = newm; j < (*m) + 4; j++) {
      for(i = 0; i < newn + 4; i++)
	mtx[i][jaux] = mtx[i][j];
      ++jaux;
    }
  }

  else {

    /*
     * forma alternativa
     */

    /* remover linhas
     */
    for(i = 0;i < *n; i++){
      
      if(shelters[mtx[i][(*m)+3] - initial_n][2] == 0) {
	--newn;
	aux3 = aux2++;
	free(*aux3);
      }
      else
	*aux1++ = *aux2++;
    }
    
    for(j = *n; j < (*n) + 4; j++)
      *aux1++ = *aux2++;
    
    
    for(j = 0, jaux = 0; j < *m; j++)
      
      if(buildings[mtx[(*n)+3][j]][2] == 0)
	--newm;
      else {
	for(i = 0; i < newn + 4; i++)
	  mtx[i][jaux] = mtx[i][j];
	++jaux;
      }

    for(jaux = newm; j < (*m) + 4; j++) {
      for(i = 0; i < newn + 4; i++)
	mtx[i][jaux] = mtx[i][j];
      ++jaux;
    }
  }


  
  *n = newn;
  *m = newm;
}


int is_matrix_solved(int **build){
  int i;
  for(i=0;i<initial_n;i++)
    if(build[i][2]!=0)
      return 0;
  return 1;
}

void copy_matrix(int **mtx, int **copy, int n, int m){
  int i;
  for(i=0;i<n+4;i++)
    memcpy(copy[i],mtx[i],sizeof(int)*(m+4));
}

int main() {
  int n, m, i;/*, total_cost = 0;*/
  int **result, **costs, **buildings, **shelters;/* **costs_copy;*/
  int idi, idj;

  scanf("%d %d",&n ,&m);

  initial_n = n;
  initial_m = m;

  n = m = 0;

  buildings = malloc(sizeof(int*)*initial_n);
  for(i = 0; i < initial_n; i++)
    buildings[i] = malloc(sizeof(int)*3);

  shelters = malloc(sizeof(int*)*initial_m);
  for(i = 0; i < initial_m; i++)
    shelters[i] = malloc(sizeof(int)*3);

  for(i = 0; i < initial_n; i++) {
    scanf("%d %d %d",&buildings[i][0],&buildings[i][1],&buildings[i][2]);
    n += buildings[i][2];
  }

  for(i = 0; i < initial_m; i++) {
    scanf("%d %d %d",&shelters[i][0],&shelters[i][1],&shelters[i][2]);
    m += shelters[i][2];
  }
  
  costs = malloc(sizeof(int*)*(n+4));
  for(i = 0; i < n+4; i++)
    costs[i] = malloc(sizeof(int)*(m+4));

  /*  costs_copy = malloc(sizeof(int*)*(n+4));
  for(i = 0; i < n+4; i++)
    costs_copy[i] = malloc(sizeof(int)*(m+4));
  */
  result = malloc(sizeof(int*)*initial_n);
  for(i = 0; i < initial_n; i++)
    result[i] = malloc(sizeof(int)*initial_m);

  init_matrix(costs,buildings,shelters,n,m);
  init_result(result,initial_n,initial_m);

  /*while(!is_matrix_solved(buildings)) {*/

  /*if(m<n){
    puts("E RODA");
    costs = rotate_matrix(costs,n,m);
    aux=m;
    m=n;
    n=aux;
    }*/
  
  /*  copy_matrix(costs,costs_copy,n,m);*/

  /*  print_matrix(costs,n+4,m+4);*/
  
  solve_matrix(costs,n,m);
  
  /*    min=getmincost(buildings,shelters,n,m);
	send_people(result,costs_copy,n,m,min);
	submincost(costs_copy,buildings,shelters,min,n,m);
	i=n;
	j=m;
        remove_full_and_empty(costs,buildings,shelters,&n,&m);*/
  /*if(costs[0][n+3] < initial_n)
    getmincost2(costs_copy,costs,n,m,&imin,&jmin);
    else
    getmincost2(costs_copy,costs,m,n,&imin,&jmin);*/
    
  for(i=0;i<n;i++) {
    /*total_cost += costs_copy[i][costs[i][m+1]];*/
    idi = costs[i][m+3];
    idj = costs[n+3][costs[i][m+1]]-initial_n;
    result[idi][idj]++;
  }


  print_result(result,initial_n,initial_m);

  free_matrix(costs,n+4);
  free_matrix(buildings,initial_n);
  free_matrix(shelters,initial_m);
  free_matrix(result,initial_n);

  /*  printf("Custo total = %d\n",total_cost);*/
  printf("Capacidade total = %d\n",n+m);
  
  return 0;
}
