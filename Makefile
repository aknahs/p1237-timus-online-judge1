CC = gcc
CFLAGS = -ansi -Wall -pedantic -o2 -g
HEADERS = proj.h
OBJS = proj.o

all: $(OBJS) $(HEADERS)
	$(CC) $(CFLAGS) -o proj $(OBJS)

clean:
	rm -f *.o *~ proj
